# Readme
Just enough to run this with iOS....

## If you haven't already
sudo npm install -g cordova ionic bower

## Platform - iOS
ionic platform ios

## Cordova Plugins 
* cordova plugin add com.ionic.keyboard
* cordova plugin add cordova-plugin-console
* cordova plugin add cordova-plugin-device
* cordova plugin add cordova-plugin-network-information
* cordova plugin add https://github.com/whiteoctober/cordova-plugin-app-version.git
* cordova plugin add https://pickettjd@bitbucket.org/pickettjd/cj-echo.git

## Brandon Notes
That last one is the one I'm working with - iOS only (right now).
If you run this with ionic emulate ios, it works, but the code that is causing me pain is commented out.

If you clone cj-echo, or just look at the plugin code, you'll see there is the cj-echo/www/echo.js file.  This is probably my problem, or where I'm calling it from:  elitescheduleapp/www/app/teams/team-detail-getAppVersion-ctrl.js

It appears to break when I uncomment the call to window.echo. 




