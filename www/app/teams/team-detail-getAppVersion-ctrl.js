(function () {
   'use strict';

   angular.module('eliteApp').controller('getAppVersionCtrl', ['$cordovaNetwork', getAppVersionCtrl]);

   function getAppVersionCtrl($cordovaNetwork) {
      var vm = this;

      console.log("isOnline", $cordovaNetwork.isOnline());       
      console.log("echo props -", Object.getOwnPropertyNames(window.echo));
      // console.log("window props -", Object.getOwnPropertyNames(window));

      window.echo.echo("ECHO Echo echo!", function(ev) {
         console.log('Returned Echo Value -', ev);
      });

      window.echo.datetime(function(dt) {
         console.log('Returned Datetime Value -', dt);
      });      
   };

})();
      