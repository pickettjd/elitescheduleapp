(function () {
   'use strict';

   angular.module('eliteApp').controller('LeaguesCtrl', ['$state', 'eliteApi',  LeaguesCtrl]);

   function LeaguesCtrl($state, eliteApi) {
      var vm = this;

      eliteApi.getLeagues().then(function(data){
         console.log('Got leagues back - ', data);

         vm.leagues = data;
      });


      vm.selectLeague = function(leagueId){
         // TODO: select correct league
         console.log("league id: ", leagueId);

         // Navigate to the teams routes
         $state.go("app.teams");
      }; 

      // eliteApi.getLeagueData().then(function(data){
      //    console.log('Got team data back - ', data);
      // });
   };

})();