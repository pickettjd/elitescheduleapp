(function () {
   'use strict';

   angular.module('eliteApp').controller('LocationsCtrl', ['eliteApi', LocationsCtrl]);

   function LocationsCtrl(eliteApi) {
      var vm = this;

      eliteApi.getLeagueData().then(function(data){
         console.log('Got locations back - ', data.locations);

         vm.locations = data.locations;
      });

   };

})();
      